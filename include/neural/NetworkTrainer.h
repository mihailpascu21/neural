//
// Created by Andi on 6/10/2021.
//

#ifndef NETWORKTRAINER_H
#define NETWORKTRAINER_H

#include "Network.h"
#include <vector>

using namespace std;

namespace neural{
    class NetworkTrainer{
    public:
        NetworkTrainer(int no_inputs, int no_outputs, vector<int> hiddenLayers);

        double train(int set_width, int train_cycles);
        vector<vector<double>> test();
        vector<vector<double>> getExpOutputs();
    private:
        Network *network;
        int no_inputs;
        int no_outputs;
        vector<int> hiddenLayers;
        int dataset_width;
        vector<vector<double>> inputs;
        vector<vector<double>> exp_outputs;

    };

}

#endif //NETWORKTRAINER_H
