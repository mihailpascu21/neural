#getting the base imagine that has already the tools for build and unit tests
FROM migen97/amd64ubuntu:1.0

MAINTAINER Mihail Pascu <mihailpascu21@gmail.com>

#Run commmands at build phase of the image
#runned all the commands with only one RUN in order to keep the 
RUN cd home && git clone https://gitlab.com/mihailpascu21/neural.git && cd neural && mkdir build && cd build && cmake .. && make && ./generate_data

#Set the working directory
WORKDIR /home/neural/build

#Run commands after a container is created
#Only one CMD can be present in the Dockerfile (https://docs.docker.com/engine/reference/builder/)
CMD ["./nn_test"]