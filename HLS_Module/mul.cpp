#include "mul.hpp"

void mul(
		double inputs[N],
		double delta,
		double learning_rate,
		double weights[N]
	)
{
	#pragma HLS INTERFACE s_axilite port=return

	loop: for(int i = 0; i < N; i++) {
	   weights[i] += inputs[i] * delta * learning_rate;
	}
}
