#ifndef MUL_H_
#define MUL_H_
#define N 4

void mul(
		double inputs[N],
		double delta,
		double learning_rate,
		double weights[N]
	);

#endif