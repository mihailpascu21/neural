#include <stdio.h>
#include <math.h>
#include "mul.hpp"


int main () {
	FILE         *fout;
	FILE         *fgold;

	double inputs[N]={1,2,3,4};
	double delta=1;
	double learning_rate=0.05;
	double weights[N]={0,0,0,0};
	double weights_gold[N]={0.05,0.1,0.15,0.2};
	mul(inputs,delta,learning_rate,weights);

	for(int i=0;i<N;i++){
		if(abs(weights[i]-weights_gold[i]) >0.01){

			printf("Gresit!w=%f /t wg=%f",weights[i],weights_gold[i]);
			return 1;
		}
	}
	return 0;
}
