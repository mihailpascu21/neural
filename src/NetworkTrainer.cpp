//
// Created by Andi on 6/10/2021.
//

#include "neural/NetworkTrainer.h"
#include <vector>

using namespace std;

namespace neural{
    NetworkTrainer::NetworkTrainer(int no_inputs,int no_outputs, vector<int> hiddenLayers) :
    no_inputs(no_inputs),
    no_outputs(no_outputs),
    hiddenLayers(hiddenLayers)
    {
        this->network = new neural::Network(this->no_inputs, this->no_outputs, this->hiddenLayers);
    }

    vector<vector<double>> NetworkTrainer::getExpOutputs(){
        return exp_outputs;
    }

    double NetworkTrainer::train(int dataset_width, int train_cycles){
        this->dataset_width = dataset_width;
        std::cout<<"----Reading the inputs from the inputs file----\n";
        inputs.resize(dataset_width);

        ifstream inputsFile("inputs.bin");
        if (!inputsFile.good()) {
            std::cout << "No inputs file found" << '\n';
        }
        for(uint8_t i=0;i<dataset_width;i++) {
            for (uint8_t j = 0; j < this->no_inputs; j++) {
                uint8_t temp;
                inputsFile.read(reinterpret_cast<char*>(&temp), sizeof(temp));
                inputs[i].push_back(temp);
            }
        }

        std::cout<<"----Reading the expected outputs from the outputs file----\n";
        exp_outputs.resize(dataset_width);

        ifstream outputsFile("outputs.bin");
        if (!outputsFile.good()) {
            std::cout << "No outputs file found" << '\n';
        }
        for(uint8_t i=0;i<dataset_width;i++) {
            for(uint8_t j=0;j<this->no_outputs;j++) {
                uint8_t temp;
                outputsFile.read(reinterpret_cast<char *>(&temp), sizeof(temp));
                exp_outputs[i].push_back(temp);
            }
        }
        std::cout<<"----Training the neural network----\n";
        double mse = network->trainMultiple(inputs, exp_outputs, 0.05, train_cycles, dataset_width);
        /*
        for(int i=0;i<train_cycles;i++) {
            mse = 0.0;
            for (int j = 0; j < dataset_width; j++) {
                mse += network->trainSingle(inputs[j], exp_outputs[j], 0.05);
            }
        }*/
        return mse/dataset_width;
    }
    vector<vector<double>> NetworkTrainer::test(){
        cout << "----Testing the neural network on the inputs that were given----\n";
        vector<vector<double>> outputs(dataset_width);
        for(int i=0;i<dataset_width;i++) {
            outputs[i] = network->run(inputs[i]);
        }
        return outputs;
    }
}

