#include <iostream>
#include <vector>
#include <chrono>
#include <iomanip>
#include "neural/Network.h"
#include "neural/NetworkTrainer.h"
using namespace std;
//
//#define DEBUG
//
const int NUM_INPUTS  = 4;
const int NUM_OUTPUTS = 1;

int main(void){
    string outputEvaluation;
    std::cout<<"----Create the Neural Network----\n";
    vector<int> hiddenLayers;
    hiddenLayers.push_back(4);
    hiddenLayers.push_back(4);
    hiddenLayers.push_back(2);

    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();
    neural::NetworkTrainer *netTrain = new neural::NetworkTrainer(NUM_INPUTS,NUM_OUTPUTS,hiddenLayers);
    std::chrono::steady_clock::time_point create_timer = std::chrono::steady_clock::now();

    std::cout << netTrain->train(16,10000) << endl;
    std::chrono::steady_clock::time_point train_timer = std::chrono::steady_clock::now();

    vector<vector<double>> outputs(16);
    vector<vector<double>> exp_outputs(16);

    outputs = netTrain->test();

    exp_outputs = netTrain->getExpOutputs();
    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();
    std::cout << "Outputs after training!"<< endl;

    std::cout << std::fixed << "|  Actual  |" << "| Expected |" << "| Deviation |" << "| Evaluation |" << endl;
    for(int i=0;i<16;i++) {
        std::cout << std::setw(10) << std::fixed << std::setprecision(5) << outputs[i][0] << " |";
        std::cout << std::setw(10) << exp_outputs[i][0] << " |";
        std::cout << std::setw(10) << abs(outputs[i][0] - exp_outputs[i][0]) << "  |";
        if(abs(outputs[i][0] - exp_outputs[i][0]) < 0.1)
            outputEvaluation = "CORRECT";
        else
            outputEvaluation = "INCORRECT";
        std::cout << std::setw(10) << outputEvaluation << endl;
    }
    auto create_computation_us = std::chrono::duration_cast<std::chrono::microseconds>(create_timer - begin).count();
    auto train_computation_us  = std::chrono::duration_cast<std::chrono::microseconds>(train_timer - create_timer).count();
    auto total_computation_us  = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count();
    std::cout << "Create Net  Time = " << create_computation_us << " us " << std::endl;
    std::cout << "Training    Time = " << train_computation_us << " us " << std::endl;;
    std::cout << "Total       Time = " << total_computation_us << " us " << std::endl;
    return 0;

}
