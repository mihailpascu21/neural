//
// Created by sabin.olaru on 5/23/2021.
//
#include <iostream>
#include <neural/Network.h>
using namespace std;
//
//#define DEBUG
//

int main(void){
    std::cout<<"----NN Data Generator----\n";

    std::cout<<"----Generating the Input file----\n";
    ofstream inputFile;
    inputFile.open ("inputs.bin", ios::binary);
    for(uint8_t i=0;i<16;i++){
        uint8_t value =i;
        for(uint8_t j=0;j<4;++j){
           //write to folder the four values;
           uint8_t temp=value%2;
           inputFile.write(reinterpret_cast<const char *>(&temp), sizeof(temp));
           value=value/2;
        }
    }
    inputFile.close();

    std::cout<<"----Generating the Output file----\n";
    ofstream outputFile;
    outputFile.open ("outputs.bin", ios::binary);
    for(uint8_t i=0;i<16;i++){
        uint8_t temp=i;
        uint8_t xorValue=0;
        for(uint8_t j=0; j<4;j++){
            xorValue^=temp%2;
            temp=temp/2;
        }
        outputFile.write(reinterpret_cast<const char *>(&xorValue), sizeof(xorValue));
#ifdef DEBUG
        cout<<"i="<<unsigned(i)<<";\tXorValue="<<unsigned(xorValue)<<endl;
#endif
    }
    outputFile.close();

    std::cout<<"----Done generating the needed files----\n";
}
