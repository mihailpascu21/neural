//
// Created by Andi on 6/10/2021.
//

#include <gtest/gtest.h>
#include "neural/Network.h"
#include "neural/NetworkTrainer.h"
#include <vector>

using namespace std;


class NetworkTests: public ::testing::Test {
protected:
    neural::NetworkTrainer * myTrainer;
    vector<int> hiddenLayers;
    int no_inputs;
    int no_outputs;
    virtual void SetUp()
    {
        hiddenLayers.push_back(4);
        hiddenLayers.push_back(4);
        hiddenLayers.push_back(2);
        no_inputs = 4;
        no_outputs = 1;
        myTrainer = new neural::NetworkTrainer(no_inputs,no_outputs,hiddenLayers);
        myTrainer->train(16, 10000);
    }

    virtual void TearDown()
    {
        delete myTrainer;
    }
};

class IndividualTests: public ::testing::Test {
protected:
    neural::Network *network;
    neural::Network *networkTest;
    vector<int> hiddenLayers;
    int no_inputs;
    int no_outputs;
    string file;
    virtual void SetUp()
    {
        hiddenLayers.push_back(4);
        hiddenLayers.push_back(4);
        hiddenLayers.push_back(2);
        file = "NetworkDetails.txt";
        no_inputs = 4;
        no_outputs = 1;
        network = new neural::Network(no_inputs,no_outputs,hiddenLayers);
    }

    virtual void TearDown()
    {
    }
};

TEST_F(NetworkTests, XOR){
    for(int i=0;i<16;i++) {
        ASSERT_NEAR(myTrainer->test()[i][0], myTrainer->getExpOutputs()[i][0], 0.1);
    }
}
TEST_F(IndividualTests, Write){
    ASSERT_TRUE(network->write(file));
}
TEST_F(IndividualTests, Read){
    ASSERT_EQ(networkTest->read(file).Layers(), network->Layers());
    ASSERT_EQ(networkTest->read(file).Inputs(), network->Inputs());
    ASSERT_EQ(networkTest->read(file).Outputs(), network->Outputs());
}
TEST_F(IndividualTests, TrainSingle){
    vector<double> input;
    input.push_back(1);
    input.push_back(0);
    input.push_back(0);
    input.push_back(1);
    vector<double> exp_output;
    exp_output.push_back(0);
    ASSERT_NE(network->trainSingle(input,exp_output,0.1), network->trainSingle(input,exp_output,0.1));
}
